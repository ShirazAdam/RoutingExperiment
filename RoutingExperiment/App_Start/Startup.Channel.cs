﻿using System.Web;
using Microsoft.Owin;
using Owin;

namespace RoutingExperiment
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureChannel(IAppBuilder app)
        {
            //app.CreatePerOwinContext()
            app.Use((context, next) =>
            {
                Channelise(context);
                return next.Invoke();
            });

        }

        private void Channelise(IOwinContext context)
        {
            //context.Request.Path = new PathString("/sa/index");
            //System.Web.Routing.RequestContext requestContext = context.Environment["System.Web.Routing.RequestContext"] as System.Web.Routing.RequestContext;
            //if (requestContext != null)
            //    requestContext.HttpContext.RewritePath("/sa/index");
            //requestContext.RouteData.Values["action"] = "injection";
            //HttpContext httpContext = HttpContext.Current;
            //httpContext.RewritePath("/sa/index");
            //throw new System.NotImplementedException();

            //return next(context);
        }
    }
}
