﻿using System;
using System.Web;
using System.Web.Mvc;

namespace RoutingExperiment.Controllers
{
    public class SAController : Controller
    {
        // GET: SA
        public ActionResult Index()
        {
            var sa = HttpContext.Request.Url.AbsoluteUri;
            //HttpContext.Request.RequestContext.HttpContext.RewritePath("/sa");
            var testers = HttpContext.Request.RequestContext.HttpContext.Request.Url;

            ViewBag.sa = sa;
            ViewBag.testers = testers;

            var uris = new Uri(HttpContext.Request.Url.AbsoluteUri);

            var urib = new UriBuilder(HttpContext.Request.Url.AbsoluteUri);

            ViewBag.rawurl = string.Format("{0}://{1}:{2}{3}", uris.Scheme, uris.DnsSafeHost, uris.Port, HttpContext.Request.RequestContext.HttpContext.Request.RawUrl);

            var u = new UrlHelper(ControllerContext.RequestContext);// (HttpContext.Request.RequestContext);
            var sss = u.RequestContext.HttpContext.Request.Url.AbsoluteUri;
            return View();
        }
    }
}