﻿using Microsoft.Owin;
using Owin;
using RoutingExperiment.Middleware;

[assembly: OwinStartupAttribute(typeof(RoutingExperiment.Startup))]
namespace RoutingExperiment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureChannel(app); //channel service
            ConfigureAuth(app);
        }
    }
}
