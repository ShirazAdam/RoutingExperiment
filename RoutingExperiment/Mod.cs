﻿using System;
using System.Web;

namespace RoutingExperiment
{
    public class Mod : IHttpModule
    {

        public Mod() { }



        public void Init(HttpApplication context)
        {
            context.BeginRequest +=
            Application_BeginRequest;
            context.EndRequest +=
                Application_EndRequest;

            //throw new NotImplementedException();
        }

        private void Application_EndRequest(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            //context.Request.RequestContext.HttpContext.RewritePath("sa/sa/1");
            //context.Context.RewritePath("sa/sa/1");
            HttpContext context = HttpContext.Current;
            context.RewritePath("/sa/index");
            //throw new NotImplementedException();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}
